import os
import whisper
import csv
model = whisper.load_model("medium")

path = "./recordings_sound_2013-long/" # try for all the audio files path
listed = os.listdir(path)
transcribed = []
for f in listed:
    f = path + f
    transcribed.append(f + "-" + model.transcribe(f)['text'])
    #print(model.transcribe(f)['text'])
for transcribe in transcribed:
    with open('transcribed.csv', 'a') as file:
        wr = csv.writer(file)
        wr.writerow([transcribe])
