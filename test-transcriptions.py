from dotenv import load_dotenv
import os
import csv
import numpy as np
import pandas as pd
import speech_recognition as sr
from scipy.io.wavfile import write
import whisper
model = whisper.load_model("medium")
import sounddevice as sd
import soundfile as sf

load_dotenv()
OPENAI_API_KEY = os.getenv("OPENAI_API_KEY")

# Multiple docs but get the relevant ones only
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.vectorstores import FAISS
from langchain.llms import OpenAI
from langchain.chains import RetrievalQA
from langchain.prompts import PromptTemplate

# path joining version for other paths
filename = "./transcribed.csv"
total_chats = len(pd.read_csv(filename))
counter = total_chats + 1

texts = []
# opening the CSV file
with open("transcribed.csv", mode="r") as file:
    # reading the CSV file
    CSVFile = csv.reader(file)
    texts = [lines for lines in CSVFile]

texts = np.asarray(texts).ravel()

embeddings = OpenAIEmbeddings()
docsearch = FAISS.from_texts(texts, embeddings)


def create_prompt_query(query):
    # Sentence matching prompt
    prompt = """Use the following pieces of context as a reply to the query at the end. If you don't know the answer, make up a {context} using similar words or context from the source file. Always reply from following pieces of context or source data file.
    {context}
    If the {context} is a question, then reply with a question. If the {context} is a joke, then reply with a joke.
    Query: {question}
    Answer in any language that you know:"""

    PROMPT = PromptTemplate(template=prompt, input_variables=["context", "question"])

    chain_type_kwargs = {"prompt": PROMPT}
    vectordbkwargs = {
        "search_distance": 0.1
    }  # predictions be more random, then higher the temperature
    qa = RetrievalQA.from_chain_type(
        llm=OpenAI(temperature=0),
        chain_type="stuff",
        retriever=docsearch.as_retriever(search_type="similarity"),
        chain_type_kwargs=chain_type_kwargs,
        return_source_documents=True,
        verbose=False,
    )  # keep verbose=True during dev stage
    result = qa({"query": query, "vectordbkwargs": vectordbkwargs})
    return result

fs = 44100  # Sample rate
seconds = 20  # Duration of recording
counter = 0

while True:

    print("Calibrating! ... ")
    print("Okay, go!")
    myrecording = sd.rec(int(seconds * fs), samplerate=fs, channels=2)
    sd.wait()  # Wait until recording is finished
    counter = counter + 1
    new_file = "./nabanita_test_recordings/recording" + str(counter) + ".wav"
    write(new_file, fs, myrecording)  # Save as WAV file

    try:
        print("Recognizing...")
        # whisper model options are found here: https://github.com/openai/whisper#available-models-and-languages
        # other speech recognition models are also available.
        query = model.transcribe(new_file)["text"]
    except Exception as e:
        unrecognized_speech_text = (
                        f"Sorry, I didn't catch that. Exception was: {e}s"
                    )
        query = unrecognized_speech_text
    print("You said : " + query)

    with open("transcribed.csv", "a") as file:
        wr = csv.writer(file)
        wr.writerow([new_file + "-" + query + "\n"])

    result = create_prompt_query(query)
    print("GPT replies : " + result["result"])
    print("Source files : " + result["source_documents"][0].page_content)
    content = result["source_documents"][0].page_content
    if content.find(".wav") > 0:
        filename = content[0:content.find(".wav")] + ".wav"
    elif content.find(".mp4") > 0:
        filename = content[0:content.find(".mp4")] + ".mp4"
    elif content.find(".mp3") > 0:
        filename = content[0:content.find(".mp3")] + ".mp3"
    else:
        filename = "File not found!"
    # Extract data and sampling rate from file
    data, fs = sf.read(filename, dtype='float32')  
    sd.play(data, fs)
    sd.wait()  # Wait until file is done playing
